# -*- coding: utf-8 -*-

import StringIO
import json
import math
import logging
import random
import urllib
import urllib2
import re

# for sending images
from PIL import Image
import multipart

# standard app engine imports
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import webapp2

TOKEN = '230008382:AAEukpbZDwCP8-QnCqZ0Y5RQGJzEibMbI-Y'

BASE_URL = 'https://api.telegram.org/bot' + TOKEN + '/'

API_URL = 'http://188.120.233.65/pizza.php/'

STATE = None # to determine if it is a registration process or user is already a client
WRONG_DATA = None # to check which data user entered incorrectly (name or address)
START_STEP = 0 # when user is already a customer we need to determine when to post greeting and list of prev orders
REGISTER_USER_STEP = 0 # for step-by-step registration
SET_USERDATA_TEXT = {} # array with entire user data
NEW_ORDER_MESSAGE_IDS = [] # ids of messages to edit
CURRENT_ORDER = [] # current order of user
CURRENT_ADDRESS = None # user's address
PRICE = 0 # order's price

GEOLOCATION_API_KEY = 'AIzaSyCSjYBswmQdOEMOWs9bqJbijkM_7_eanzM' # google maps api key

# ================================

class EnableStatus(ndb.Model):
    # key name: str(chat_id)
    enabled = ndb.BooleanProperty(indexed=False, default=False)


# ================================

def setEnabled(chat_id, Si):
    es = EnableStatus.get_or_insert(str(chat_id))
    es.enabled = Si
    es.put()

def getEnabled(chat_id):
    es = EnableStatus.get_by_id(str(chat_id))
    if es:
        return es.enabled
    return False



# ================================

class MeHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getMe'))))


class GetUpdatesHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))))


class SetWebhookHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        url = self.request.get('url')
        if url:
            self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))))


class WebhookHandler(webapp2.RequestHandler):
    def post(self):
        def reply(chat_id, msg=None, img=None, reply_markup=None, inline_keyboard=None, add=None):
            global NEW_ORDER_MESSAGE_IDS
            if msg and reply_markup:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                    'disable_web_page_preview': 'false',
                    'reply_markup': reply_markup,
                })).read()
                response = json.loads(resp)
                message_id = response['result']['message_id']
                if add is None:
                    NEW_ORDER_MESSAGE_IDS.append(str(message_id))
                logging.info('RESPONSE: ' + str(message_id))
            elif msg:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                    # 'text': msg.encode('utf-8'),
                    'disable_web_page_preview': 'false',
                })).read()
                response = json.loads(resp)
                message_id = response['result']['message_id']
                if add is None:
                    NEW_ORDER_MESSAGE_IDS.append(str(message_id))
                logging.info('RESPONSE: ' + str(message_id))
            elif img:
                resp = multipart.post_multipart(BASE_URL + 'sendPhoto', [
                    ('chat_id', str(chat_id)),
                ], [('photo', 'image.jpg', img), ])
            else:
                logging.error('no msg or img specified')
                resp = None

            logging.info('send response:')
            logging.info(resp)

        def editMessage(chat_id, message_id, msg=None, img=None, reply_markup=None, inline_keyboard=None):
            global NEW_ORDER_MESSAGE_IDS
            if msg and reply_markup:
                resp = urllib2.urlopen(BASE_URL + 'editMessageText', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'message_id': int(message_id),
                    'text': msg,
                    'disable_web_page_preview': 'false',
                    'reply_markup': reply_markup,
                })).read()
                response = json.loads(resp)
                message_id = response['result']['message_id']
                NEW_ORDER_MESSAGE_IDS.append(message_id)
                logging.info('RESPONSE: ' + str(message_id))
            elif msg:
                resp = urllib2.urlopen(BASE_URL + 'editMessageText', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'message_id': int(message_id),
                    'text': msg,
                    # 'text': msg.encode('utf-8'),
                    'disable_web_page_preview': 'false',
                })).read()
                response = json.loads(resp)
                message_id = response['result']['message_id']
                NEW_ORDER_MESSAGE_IDS.append(message_id)
                logging.info('RESPONSE: ' + str(message_id))
            elif img:
                resp = multipart.post_multipart(BASE_URL + 'sendPhoto', [
                    ('chat_id', str(chat_id)),
                ], [('photo', 'image.jpg', img), ])
            else:
                logging.error('no msg or img specified')
                resp = None

            logging.info('send response:')
            logging.info(resp)

        def getPizzas():
            url = API_URL + '?getPizzas=GET'
            response = urllib2.urlopen(url)
            data = json.load(response)
            pizzas = []
            for pizza in data:
                name = str((pizza['Name']).encode('utf8'))
                picture = str((pizza['Picture']).encode('utf8'))
                price = str((pizza['Price']).encode('utf8'))

                pizzas.append({
                    'Id': pizza['ID'],
                    'Name': name,
                    'Picture' : picture,
                    'Price' : price,
                })
            return pizzas

        def getPizza(name):
            pizzas = getPizzas()
            for pizza in pizzas:
                if (pizza['Name'] == name):
                    return pizza
            return None

        def CheckClient(username):
            url = API_URL + '?getClientByUsername=GET&Username=' + username
            response = urllib2.urlopen(url)
            data = json.load(response)
            return data != []

        def GetUser(username):
            url = API_URL + '?getClientByUsername=GET&Username=' + username
            response = urllib2.urlopen(url)
            data = json.load(response)
            logging.info(username)
            fullName = str((data['FullName']).encode('utf8'))
            defaultAddress = u''.join(data['DefaultAddress']).encode('utf8').strip()
            return {
                'FullName': fullName,
                'DefaultAddress': defaultAddress
            }

        def addOrder(orderdata):
            values = orderdata
            values.update({'addOrder': 'ADD'})
            data = urllib.urlencode(values, True)
            req = urllib2.Request(API_URL, data)
            response = urllib2.urlopen(req)
            return response.read()


        def registerUser(userdata):
            values = userdata
            values.update({'addClient': 'ADD'})
            data = urllib.urlencode(values)
            req = urllib2.Request(API_URL, data)
            response = urllib2.urlopen(req)
            logging.info(values)
            logging.info(response.read())
            return 'Success'

        def getLastOrders(username):
            url = API_URL + '?getClientsOrders=GET&Username=' + username
            response = urllib2.urlopen(url)
            data = json.load(response)
            lastOdrers = []
            for order in data:
                address = u''.join((order['Address'])).encode('utf8').strip()
                lastOdrers.append({'Address': address, 'Pizzas': []})
                for pizza in order['Pizzas']:
                    lastOdrers[-1]['Pizzas'].append(pizza)
            return lastOdrers

        def getPlaceByCoords(latitude, longitude):
            url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + str(latitude) + ',' + str(
                longitude) + '&key=' + GEOLOCATION_API_KEY
            response = urllib2.urlopen(url)
            data = json.load(response)
            return data.get('results')[0].get('formatted_address')

        global STATE
        global REGISTER_USER_STEP
        global START_STEP
        global SET_USERDATA_TEXT
        global WRONG_DATA
        global CURENT_PAGE
        global COUNT_OF_PIZZAS
        global COUNT_OF_PIZZAS_ON_PAGE
        global NEW_ORDER_MESSAGE_IDS
        global CURRENT_ORDER
        global CURRENT_ADDRESS
        global PRICE

        BASKET = []
        urlfetch.set_default_fetch_deadline(60)
        body = json.loads(self.request.body)
        logging.info('request body:')
        logging.info(body)
        self.response.write(json.dumps(body))

        update_id = body['update_id']

        if 'callback_query' in body:
            data = body['callback_query']['data']
            message = body['callback_query']['message']
            message_id = message['message_id']
            fr = message['from']
            chat = message['chat']
            chat_id = chat['id']
            callback = data.split(' ')
            type = callback[0]


            if type == 'last_orders':
                callback_data = callback[1]
                last_order_id = callback_data
                last_orders = getLastOrders(chat['username'])
                pizzas = ''
                for pizza in last_orders[int(last_order_id)]['Pizzas']:
                    pizzas += pizza + ' '
                orderdata = {
                    'Username': chat['username'],
                    'Address': str(last_orders[int(last_order_id)]['Address']),
                    'Pizzas': json.dumps(last_orders[int(last_order_id)]['Pizzas'])
                }
                logging.info(addOrder(orderdata))
                reply(chat_id, 'You	have ordered ' + pizzas +
                  'for ' + str(GetUser(chat['username'])['FullName']) + ' to be deliverd in ' + str(last_orders[int(last_order_id)]['Address']))
           # reply(chat_id, str(body['callback_query']['data']))
            if type == 'add':
                callback_data = ' '.join(callback[1:]).encode('utf-8')
                CURRENT_ORDER.append(callback_data)
                PRICE += float(getPizza(callback_data)['Price'])
                reply(chat_id, 'Quieres pedir una ' + str(callback_data) + ' por $' + str(getPizza(str(callback_data))['Price']) + '? A cancelar por efectivo?', add=True)
                logging.info(CURRENT_ADDRESS)
            if type == 'confirm':
                logging.info(CURRENT_ADDRESS)
                callback_data = callback[1]
                if CURRENT_ORDER == []:
                    reply(chat_id, 'Your order is empty')
                elif callback_data == 'Si':
                    orderdata = {
                        'Username' : chat['username'],
                        'Address': CURRENT_ADDRESS,
                        'Pizzas' : json.dumps(CURRENT_ORDER)
                    }
                    logging.info(CURRENT_ORDER)
                    logging.info(orderdata)
                    logging.info(addOrder(orderdata))
                    reply(chat_id, 'Your order has been taken, it will be delivered in 30 min or it’s free ' + CURRENT_ADDRESS)
                    # reply(chat_id, 'Tu orden has sido recibida. Una (pizza) then price)) sera enviada a (name) a la direccion (addrees). Se entregara en 30 minutos o es gratis. Para cualquier consulta puedes llamar al 2257-7777' + CURRENT_ADDRESS)

                elif callback_data == 'No':
                    PRICE = 0
                else:
                    strPizzaOrder = ''
                    for pizza in CURRENT_ORDER:
                        strPizzaOrder += pizza + ' '
                    row = []
                    SiButton = {
                        'text': 'Si',
                        'callback_data': 'confirm Si'
                    }
                    noButton = {
                        'text': 'No',
                        'callback_data': 'newOrder newOrder'
                    }
                    row.append(SiButton)
                    row.append(noButton)
                    keyboardInline = [row]
                    inlineMarkup = {'inline_keyboard': keyboardInline}
                    reply(chat_id, 'Your order: ' + strPizzaOrder + '\nPrice: $' + str(PRICE))
                    reply(chat_id, 'Confirmar?', reply_markup=json.dumps(inlineMarkup))

            if type == 'newOrder':
                callback_data = callback[1]
                if callback_data == 'newOrder':
                    CURRENT_ORDER = []
                    CURENT_PAGE = 0
                    pizzas = getPizzas()
                    keyboard = [['Mas', 'Confirmando orden']]
                    replyMarkup = {'keyboard': keyboard}
                    reply(chat_id, 'Espere un momento', reply_markup=json.dumps(replyMarkup))
                    for i in range (CURENT_PAGE * 3, CURENT_PAGE * 3 + 3):
                        row = []
                        orderButton = {
                            'text': 'Adicionar',
                            'callback_data': 'add ' + str(pizzas[i]['Name'])
                        }

                        row.append(orderButton)
                        keyboardInline = [row]
                        inlineMarkup = {'inline_keyboard': keyboardInline}
                        reply(chat_id, img=urllib2.urlopen('http://188.120.233.65' + str(pizzas[i]['Picture'])).read())
                        reply(chat_id, pizzas[i]['Name'], reply_markup=json.dumps(inlineMarkup))
                    CURENT_PAGE = CURENT_PAGE + 1
                    STATE = ''
            return
            # if type == 'newOrder':
            #     callback_data = callback[1]
            #     if callback_data == 'newOrder':
            #         CURRENT_ORDER = []
            #         CURENT_PAGE = 0
            #         pizzas = getPizzas()
            #         COUNT_OF_PIZZAS = len(pizzas)
            #         for i in range(5, 0, -1):
            #             if COUNT_OF_PIZZAS % i == 0:
            #                 COUNT_OF_PIZZAS_ON_PAGE = i
            #         # if (COUNT_OF_PIZZAS - 3 * (CURENT_PAGE + 1)) >= 0:
            #         #     COUNT_OF_PIZZAS_ON_PAGE = 3
            #         # else:
            #         #     COUNT_OF_PIZZAS_ON_PAGE = 3 + (COUNT_OF_PIZZAS - 3 * (CURENT_PAGE + 1))
            #         if COUNT_OF_PIZZAS_ON_PAGE == 1:
            #             row = []
            #             orderButton = {
            #                 'text': 'Adicionar',
            #                 'callback_data': 'add ' + str(pizzas[CURENT_PAGE]['Name'])
            #             }
            #
            #             row.append(orderButton)
            #             keyboardInline = [row]
            #             inlineMarkup = {'inline_keyboard': keyboardInline}
            #             # reply(chat_id, img=urllib2.urlopen('http://188.120.233.65' + str(pizzas[CURENT_PAGE]['Picture'])).read())
            #             reply(chat_id, str(str(pizzas[CURENT_PAGE]['Name'])) + '\n$' + (str(pizzas[CURENT_PAGE]['Price'])),
            #                   reply_markup=json.dumps(inlineMarkup))
            #             NEW_ORDER_MESSAGE_IDS.append(message_id)
            #         else:
            #             for j in range(COUNT_OF_PIZZAS_ON_PAGE):
            #                 row = []
            #                 orderButton = {
            #                     'text': 'Adicionar',
            #                     'callback_data': 'add ' + str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Name'])
            #                 }
            #
            #                 row.append(orderButton)
            #                 keyboardInline = [row]
            #                 inlineMarkup = {'inline_keyboard': keyboardInline}
            #                 # reply(chat_id, img=urllib2.urlopen('http://188.120.233.65' + str(pizzas[CURENT_PAGE]['Picture'])).read())
            #                 reply(chat_id, str(str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Name'])) + '\n$' + (str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Price'])), reply_markup=json.dumps(inlineMarkup))
            #                 NEW_ORDER_MESSAGE_IDS.append(message_id)
            #         row = []
            #         previousPageButton = {
            #             'text': '<',
            #             'callback_data': 'newOrder prevPage'
            #         }
            #         nextPageButton = {
            #             'text': '>',
            #             'callback_data': 'newOrder nextPage'
            #         }
            #         confirmButton = {
            #             'text': 'Confirm Order',
            #             'callback_data': 'confirm confirmOrder'
            #         }
            #         row.append(previousPageButton)
            #         row.append(nextPageButton)
            #         row.append(confirmButton)
            #         keyboardInline = [row]
            #         inlineMarkup = {'inline_keyboard': keyboardInline}
            #         reply(chat_id, 'Choose Pizza', reply_markup=json.dumps(inlineMarkup))
            #         logging.info(CURRENT_ADDRESS)
            #     if callback_data == 'prevPage':
            #         if CURENT_PAGE == 0:
            #             return
            #         CURENT_PAGE -= 1
            #         NEW_ORDER_MESSAGE_IDS.sort()
            #         last_ids = NEW_ORDER_MESSAGE_IDS[-COUNT_OF_PIZZAS_ON_PAGE:]
            #         pizzas = getPizzas()
            #         COUNT_OF_PIZZAS = len(pizzas)
            #         for i in range(5, 0, -1):
            #             if COUNT_OF_PIZZAS % i == 0:
            #                 COUNT_OF_PIZZAS_ON_PAGE = i
            #
            #
            #         # if (COUNT_OF_PIZZAS - 3 * (CURENT_PAGE + 1)) >= 0:
            #         #     COUNT_OF_PIZZAS_ON_PAGE = 3
            #         # else:
            #         #     COUNT_OF_PIZZAS_ON_PAGE = 3 + (COUNT_OF_PIZZAS - 3 * (CURENT_PAGE + 1))
            #         if COUNT_OF_PIZZAS_ON_PAGE == 1:
            #             row = []
            #             orderButton = {
            #                 'text': 'Adicionar',
            #                 'callback_data': 'add ' + str(pizzas[CURENT_PAGE]['Name'])
            #             }
            #
            #             row.append(orderButton)
            #             keyboardInline = [row]
            #             inlineMarkup = {'inline_keyboard': keyboardInline}
            #             # editMessage(chat_id, int(last_ids[0]) - 1, str(str(pizzas[CURENT_PAGE]['Name'])),reply_markup=json.dumps(inlineMarkup))
            #             editMessage(chat_id, int(last_ids[0]) - 1, str(str(pizzas[CURENT_PAGE]['Name'])) + '\n$' + (str(pizzas[CURENT_PAGE]['Price'])), reply_markup=json.dumps(inlineMarkup))
            #             NEW_ORDER_MESSAGE_IDS.append(message_id)
            #         else:
            #             for j in range(1, COUNT_OF_PIZZAS_ON_PAGE):
            #                 row = []
            #                 orderButton = {
            #                     'text': 'Adicionar',
            #                     'callback_data': 'add ' + str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Name']).encode('utf-8')
            #                 }
            #
            #                 row.append(orderButton)
            #                 keyboardInline = [row]
            #                 inlineMarkup = {'inline_keyboard': keyboardInline}
            #                 logging.info('CURRENT LAST ID: ' + str(last_ids[j]))
            #                 editMessage(chat_id, int(last_ids[j]) - 1, str(str(pizzas[CURENT_PAGE]['Name'])) + '\n$' + (str(pizzas[CURENT_PAGE]['Price'])),
            #                       reply_markup=json.dumps(inlineMarkup))
            #                 NEW_ORDER_MESSAGE_IDS.append(message_id)
            #         row = []
            #         previousPageButton = {
            #             'text': '<',
            #             'callback_data': 'newOrder prevPage'
            #         }
            #         row.append(previousPageButton)
            #         nextPageButton = {
            #             'text': '>',
            #             'callback_data': 'newOrder nextPage'
            #         }
            #         confirmButton = {
            #             'text': 'Confirm Order',
            #             'callback_data': 'confirm confirmOrder'
            #         }
            #
            #         row.append(nextPageButton)
            #         row.append(confirmButton)
            #         keyboardInline = [row]
            #         inlineMarkup = {'inline_keyboard': keyboardInline}
            #         # editMessage(chat_id, int(last_ids[-1]), 'Choose Pizza', reply_markup=json.dumps(inlineMarkup))
            #     elif callback_data == 'nextPage':
            #         if CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE == COUNT_OF_PIZZAS:
            #             return
            #         CURENT_PAGE += 1
            #         NEW_ORDER_MESSAGE_IDS.sort()
            #         last_ids = NEW_ORDER_MESSAGE_IDS[-COUNT_OF_PIZZAS_ON_PAGE:]
            #         pizzas = getPizzas()
            #         COUNT_OF_PIZZAS = len(pizzas)
            #         for i in range(5, 0, -1):
            #             if COUNT_OF_PIZZAS % i == 0:
            #                 COUNT_OF_PIZZAS_ON_PAGE = i
            #         # if (COUNT_OF_PIZZAS - 3 * (CURENT_PAGE + 1)) >= 0:
            #         #     COUNT_OF_PIZZAS_ON_PAGE = 3
            #         # else:
            #         #     COUNT_OF_PIZZAS_ON_PAGE = 3 + (COUNT_OF_PIZZAS - 3 * (CURENT_PAGE + 1))
            #         if COUNT_OF_PIZZAS_ON_PAGE == 1:
            #             row = []
            #             orderButton = {
            #                 'text': 'Adicionar',
            #                 'callback_data': 'add ' + str(pizzas[CURENT_PAGE]['Name'])
            #             }
            #
            #             row.append(orderButton)
            #             keyboardInline = [row]
            #             inlineMarkup = {'inline_keyboard': keyboardInline}
            #             editMessage(chat_id, int(last_ids[0]) - 1, str(str(pizzas[CURENT_PAGE]['Name'])) + '\n$' + (str(pizzas[CURENT_PAGE]['Price'])),
            #                   reply_markup=json.dumps(inlineMarkup))
            #             NEW_ORDER_MESSAGE_IDS.append(message_id)
            #         else:
            #             for j in range(1, COUNT_OF_PIZZAS_ON_PAGE):
            #                 row = []
            #                 orderButton = {
            #                     'text': 'Adicionar',
            #                     'callback_data': 'add ' +  str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Name'])
            #                 }
            #
            #                 row.append(orderButton)
            #                 keyboardInline = [row]
            #                 inlineMarkup = {'inline_keyboard': keyboardInline}
            #                 logging.info('CURRENT LAST ID: ' + str(last_ids[j]))
            #                 # editMessage(chat_id, int(last_ids[j]) - 1, str(str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Name'])), reply_markup=json.dumps(inlineMarkup))
            #                 editMessage(chat_id, int(last_ids[j]) - 1, str(str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Name'])) + '\n$' + (str(pizzas[CURENT_PAGE * COUNT_OF_PIZZAS_ON_PAGE + j]['Price'])), reply_markup=json.dumps(inlineMarkup))
            #                 NEW_ORDER_MESSAGE_IDS.append(message_id)
            #         row = []
            #         previousPageButton = {
            #             'text': '<',
            #             'callback_data': 'newOrder prevPage'
            #         }
            #         row.append(previousPageButton)
            #
            #         nextPageButton = {
            #         'text': '>',
            #         'callback_data': 'newOrder nextPage'
            #         }
            #         row.append(nextPageButton)
            #         confirmButton = {
            #             'text': 'Confirm Order',
            #             'callback_data': 'confirm confirmOrder'
            #         }
            #
            #         row.append(confirmButton)
            #         keyboardInline = [row]
            #         inlineMarkup = {'inline_keyboard': keyboardInline}
            #         # editMessage(chat_id, int(last_ids[-1]), 'Choose Pizza', reply_markup=json.dumps(inlineMarkup))
            #
            #
            #
            #   #  reply(chat_id, 'Fok u')
            # return

        message = body['message']
        message_id = message.get('message_id')
        date = message.get('date')
        text = message.get('text')
        location = message.get('location')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']


        if not text and not location:
            logging.info('no text and no location')
            return
        if not text:
            text = ''

        if text.startswith('/'):
            if text.lower() == '/start':
                setEnabled(chat_id, True)
                NEW_ORDER_MESSAGE_IDS = []
                reply(chat_id, 'Escribe /pedir para realizar tu pedido')
                return
            elif text.lower() == '/pedir':
                if CheckClient(fr['username']):
                    STATE = 'ALREADY_A_CLIENT'
                    START_STEP = 0
                    NEW_ORDER_MESSAGE_IDS = []
                else:
                    reply(chat_id, 'Puede darme su nombre completo?')
                    STATE = 'REGISTER_USER'
                    REGISTER_USER_STEP = 0

            elif text.lower() == '/pizzas':
                pizzas = getPizzas()
                for pizza in pizzas:
                    reply(chat_id, pizza['Name'])
            elif text.lower() == '/stop':
                reply(chat_id, 'Bot disabled')
                setEnabled(chat_id, False)
            else:
                reply(chat_id, 'What command?')

        # CUSTOMIZE FROM HERE
        if STATE == 'REGISTER_USER':
            if REGISTER_USER_STEP == 1:
                fullname = str(text.encode('utf-8'))
                if fr['username'] is not None:
                    SET_USERDATA_TEXT.update({'Username' : str(fr['username'].encode('utf-8'))})
                SET_USERDATA_TEXT.update({'FullName' : fullname.encode('utf-8')})
                keyboardButtons = [[{
                    'text' : 'Send current location',
                    'request_location' : True
                                    }], [{'text' : 'Enter address'}]]
                replyMarkup = { 'keyboard' : keyboardButtons, 'one_time_keyboard' : True }
                reply(chat_id, 'Gracias, puede enviarme la direccion de donde quiere que le entregemos el pedio? O escriba la direccion, por favor.', reply_markup=json.dumps(replyMarkup))
            elif REGISTER_USER_STEP == 2:
                if text == 'Enter address':
                    reply(chat_id, 'Type your address, please')
                    REGISTER_USER_STEP = 2
                    return
                else:
                    location = message.get('location')
                    if location is not None:
                        SET_USERDATA_TEXT['DefaultAddress'] = u''.join(getPlaceByCoords(location['latitude'], location['longitude'])).encode('utf-8').strip()
                    else:
                        SET_USERDATA_TEXT['DefaultAddress'] = text.encode('utf-8')
                    keyboardButtons = [['Si'], ['Wrong name', 'Wrong address']]
                    replyMarkup = { 'keyboard' : keyboardButtons, 'one_time_keyboard' : True }
                    reply(chat_id, 'Sus datos:')
                    reply(chat_id, 'Usuario: ' + SET_USERDATA_TEXT['Username'])
                    reply(chat_id, 'Nombre completo: ' + SET_USERDATA_TEXT['FullName'])
                    reply(chat_id, 'Direccion de residencia: ' + SET_USERDATA_TEXT['DefaultAddress'])
                    reply(chat_id, 'Es correcto?', reply_markup=json.dumps(replyMarkup))
            elif REGISTER_USER_STEP == 3:
                if text == 'Si':
                    if registerUser(SET_USERDATA_TEXT) == 'Success':
                        STATE = 'ALREADY_A_CLIENT'
                        START_STEP = -1
                        keyboardButtons = [['Si', 'No']]
                        replyMarkup = {'keyboard': keyboardButtons, 'one_time_keyboard': True}
                        reply(chat_id, 'Hola, ' + GetUser(fr['username'])['FullName'] + str(', will you be ordering from '.encode('utf8')) + GetUser(fr['username'])['DefaultAddress'] + str('?').encode('utf-8'), reply_markup=json.dumps(replyMarkup))
                        REGISTER_USER_STEP = 0
                    # reply(chat_id, registerUser(SET_USERDATA_TEXT))
                if text == 'Wrong name':
                    WRONG_DATA = 'NAME'
                    reply(chat_id, 'Type it again, please')
                if text == 'Wrong address':
                    WRONG_DATA = 'Address'
                    reply(chat_id, 'Type your address')
            elif REGISTER_USER_STEP == 4 and WRONG_DATA == 'NAME':
                fullname = str(text.encode('utf-8'))
                SET_USERDATA_TEXT.update({'FullName' : fullname})

                keyboardButtons = [['Si'], ['Wrong name', 'Wrong address']]
                replyMarkup = { 'keyboard' : keyboardButtons, 'one_time_keyboard' : True }
                reply(chat_id, 'Sus datos:')
                reply(chat_id, 'Usuario: ' + SET_USERDATA_TEXT['Username'])
                reply(chat_id, 'Nombre completo: ' + SET_USERDATA_TEXT['FullName'])
                reply(chat_id, 'Direccion de residencia: ' + SET_USERDATA_TEXT['DefaultAddress'])
                reply(chat_id, 'Es correcto?', reply_markup=json.dumps(replyMarkup))

                REGISTER_USER_STEP = 2
            elif REGISTER_USER_STEP == 4 and WRONG_DATA == 'Address':
                address = str(text.encode('utf-8'))
                SET_USERDATA_TEXT.update({'DefaultAddress' : address})

                keyboardButtons = [['Si'], ['Wrong name', 'Wrong address']]
                replyMarkup = { 'keyboard' : keyboardButtons, 'one_time_keyboard' : True }
                reply(chat_id, 'Sus datos:')
                reply(chat_id, 'Usuario: ' + SET_USERDATA_TEXT['Username'])
                reply(chat_id, 'Nombre completo: ' + SET_USERDATA_TEXT['FullName'])
                reply(chat_id, 'Direccion de residencia: ' + SET_USERDATA_TEXT['DefaultAddress'])
                reply(chat_id, 'Es correcto?', reply_markup=json.dumps(replyMarkup))

                REGISTER_USER_STEP = 2

            REGISTER_USER_STEP += 1
            logging.info(REGISTER_USER_STEP)
        elif STATE == 'ALREADY_A_CLIENT':
            if START_STEP == 0:
                keyboardButtons = [['Si', 'No']]
                replyMarkup = { 'keyboard' : keyboardButtons, 'one_time_keyboard' : True }
                reply(chat_id, 'Hola, ' + GetUser(fr['username'])['FullName'] + str(', para este pedido se estara entregando en '.encode('utf8')) + GetUser(fr['username'])['DefaultAddress'] + str('?').encode('utf-8'), reply_markup=json.dumps(replyMarkup))
                START_STEP = -1
            elif text == 'Si':
                if START_STEP == -1:
                    CURRENT_ADDRESS = GetUser(fr['username'])['DefaultAddress']
                lastOrders = getLastOrders(fr['username'])
                lastOrdersStr = ""
                row = []
                if lastOrders == []:
                    lastOrdersStr = 'No ha hecho ordenes con nosotros anteriormente. Presione el boton de Nueva orden para crear una orden.'
                else:
                    numberOfOrder = 1
                    if len(lastOrders) > 3:
                        lastOrders = lastOrders[-3:]
                    for order in lastOrders:
                        lastOrdersStr += str(numberOfOrder) + '. ' + order['Address'] + '\n\n Pizzas:\n'
                        for pizza in order['Pizzas']:
                            lastOrdersStr += str(pizza).encode('utf-8') + '\n'
                        lastOrdersStr += '\n\n'
                        numberOfOrder += 1
                    for i in range(numberOfOrder - 1):
                        testButton = {
                            'text' : str(i + 1),
                            'callback_data' : 'last_orders ' + str(i)
                        }
                        row.append(testButton)
                newOrderButton = {
                    'text': 'Nueva orden',
                    'callback_data': 'newOrder newOrder'
                }
                row.append(newOrderButton)
                keyboardInline = [row]
                inlineMarkup = {'inline_keyboard': keyboardInline }
                reply(chat_id, lastOrdersStr, reply_markup=json.dumps(inlineMarkup))
            elif text == 'No':
                reply(chat_id, 'Enter new address')
            else:
                CURRENT_ADDRESS = u''.join(text).encode('utf8').strip()
                logging.info(CURRENT_ADDRESS)
                keyboardButtons = [['Si' , 'No']]
                replyMarkup = {'keyboard': keyboardButtons, 'one_time_keyboard': True}
                reply(chat_id, 'Will you be ordering from '.encode('utf8') + CURRENT_ADDRESS + str('?').encode('utf-8'), reply_markup=json.dumps(replyMarkup))
                START_STEP = -2

        elif text == 'Mas':
            pizzas = getPizzas()
            keyboard = [['Mas', 'Confirmando orden']]
            replyMarkup = {'keyboard': keyboard}
            reply(chat_id, 'Espere un momento', reply_markup=json.dumps(replyMarkup))
            for i in range(CURENT_PAGE * 3, CURENT_PAGE * 3 + 3):
                if i >= len(pizzas):
                    keyboard = [['Confirmando orden']]
                    replyMarkup = {'keyboard': keyboard}
                    reply(chat_id, 'No More Pizzas', reply_markup=json.dumps(replyMarkup))
                    break
                row = []
                orderButton = {
                    'text': 'Adicionar',
                    'callback_data': 'add ' + str(pizzas[i]['Name'])
                }

                row.append(orderButton)
                keyboardInline = [row]
                inlineMarkup = {'inline_keyboard': keyboardInline}
                reply(chat_id, img=urllib2.urlopen('http://188.120.233.65' + str(pizzas[i]['Picture'])).read())
                reply(chat_id, pizzas[i]['Name'], reply_markup=json.dumps(inlineMarkup))
            CURENT_PAGE = CURENT_PAGE + 1
        elif text == 'Confirmando orden':
            logging.info(CURRENT_ADDRESS)
            if CURRENT_ORDER == []:
                reply(chat_id, 'Your order is empty')
            else:
                confirmStr = 'Ha ordenado: \n'
                totalCost = 0
                for pizza in CURRENT_ORDER:
                    confirmStr += pizza + ' ($' + str(getPizza(pizza)['Price']) + ')\n'
                    totalCost += float(getPizza(pizza)['Price'])
                confirmStr += 'Total: $' + str(totalCost) + '\nConfirmar?'
                keyboard = [['Confirmar', 'Cambiar']]
                replyMarkup = {'keyboard': keyboard}
                reply(chat_id, confirmStr, reply_markup=json.dumps(replyMarkup))

            return
        elif text == 'Confirmar':
            orderdata = {
                'Username': chat['username'],
                'Address': CURRENT_ADDRESS,
                'Pizzas': json.dumps(CURRENT_ORDER)
            }

            logging.info(CURRENT_ORDER)
            logging.info(orderdata)
            logging.info(addOrder(orderdata))
            confirmStr = 'Tu orden has sido recibida. Una\n'
            totalCost = 0
            for pizza in CURRENT_ORDER:
                confirmStr += pizza + ' ($' + str(getPizza(pizza)['Price']) + ')\n'
                totalCost += float(getPizza(pizza)['Price'])
            confirmStr += 'sera enviada a ' + GetUser(chat['username'])['FullName'] + ' a la direccion ' + CURRENT_ADDRESS + '. Se entregara en 30 minutos o es gratis. Para cualquier consulta puedes llamar al 2257-7777.'
            reply(chat_id, confirmStr)
            reply(chat_id, 'Para pedir escriba /pedir')

        elif text == 'Cambiar':
            CURRENT_ORDER = []
            reply(chat_id, 'Your order is now empty, select pizzas once again')
        else:
            if getEnabled(chat_id):
                reply(chat_id, 'I got your message! (but I do not know how to answer)')
            else:
                logging.info('not enabled for chat_id {}'.format(chat_id))


app = webapp2.WSGIApplication([
    ('/me', MeHandler),
    ('/updates', GetUpdatesHandler),
    ('/set_webhook', SetWebhookHandler),
    ('/webhook', WebhookHandler),
], debug=True)
